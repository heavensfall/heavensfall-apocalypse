﻿using UnityEngine;
using System.Collections;

public class powerUps : MonoBehaviour 
{
	Animator gunAnimator;
	public bool doubleShotVar;
	public bool quickShot;
	public bool powerShot;
	int doubleShotTimer;
	int quickShotTimer;
	int powerShotTimer;
	PlayerHealth playerHealth;
	public AudioClip powerUpSFX;
	BaseShield baseshield;
	int ShotHash = Animator.StringToHash ("doubleShot"); 
	// Use this for initialization
	void Start () 
	{
		gunAnimator =GameObject.Find ("PlayerGun").GetComponent<Animator> ();
		baseshield = GameObject.Find ("BaseShield").GetComponent<BaseShield> ();
		playerHealth = GameObject.Find ("PlayerShip").GetComponent<PlayerHealth> ();
		gunAnimator.SetBool (ShotHash,false);
		doubleShotVar = false;
		quickShot = false;
		powerShot = false;
	}
	void Update()
	{
		if ((doubleShotVar == true) && (doubleShotTimer >= 960)) 
		{
			doubleShotVar = false;
			gunAnimator.SetBool (ShotHash,false);
		} 
		else 
		{
			doubleShotTimer++;
		}
		if ((quickShot == true) && (quickShotTimer >= 960)) 
		{
			quickShot = false;
		} 
		else 
		{
			quickShotTimer++;
		}
		if ((powerShot == true) && (powerShotTimer >= 960)) 
		{
			powerShot = false;
		} 
		else 
		{
			powerShotTimer++;
		}
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{

		switch (other.name)
		{
		case "2ShotPowerUp(Clone)":
			gunAnimator.SetBool (ShotHash,true);
			doubleShotVar = true;
			Destroy (other.gameObject);
			doubleShotTimer = 0;
			audio.PlayOneShot (powerUpSFX);
			break;

		case "quickShotPowerUp(Clone)":
			quickShot = true;
			Destroy (other.gameObject);
			quickShotTimer = 0;
			audio.PlayOneShot (powerUpSFX);
			break;

		case "powerShotPowerUp(Clone)":
			powerShot = true;
			Destroy (other.gameObject);
			powerShotTimer = 0;
			audio.PlayOneShot (powerUpSFX);
			break;

		case "shieldRestore(Clone)":
			if(baseshield.shieldHitPoints !=100)
			{
				baseshield.shieldHitPoints++;
			}
			Destroy (other.gameObject);
			audio.PlayOneShot (powerUpSFX);
			break;

		case "hpRestore(Clone)":
			if(playerHealth.playerHitPoints !=3)
			{
				playerHealth.playerHitPoints++;
			}
			Destroy (other.gameObject);
			audio.PlayOneShot (powerUpSFX);
			break;

		}
	}
}
