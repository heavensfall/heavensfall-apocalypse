﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemySpawner : MonoBehaviour
{
	const int MAX_ENEMIES = 6;
	int waveNumber;
	int enemiesSpawned;
	int powerUpID;
	public Vector3 enemyDestroyLocation;
	public int enemiesDestroyed;
	List<int[]> waveData;

	public GameObject enemyLightFighter;
	public GameObject enemyLightCruiser;
	public GameObject enemyArmouredFighter;
	public GameObject enemyBruiser;
	public GameObject enemyHeavyFighter;
	public GameObject enemyTank;
	public GameObject doubleShot;
	public GameObject powerShot;
	public GameObject shieldRestore;
	public GameObject hpRestore;
	public GameObject quickShot;
	public GameObject auxShield;
	public GameObject immunityShield;
	public Vector3 spawnPosition;

	public Sprite[] waveImages = new Sprite[6];
	public GameObject waveIndicator;

	BaseShield baseShield;

	public bool playerDead = false;

	void Start ()
	{ spawnPosition = transform.localPosition;
		//these numbers correlate to the order in which the enemies spawn
		waveIndicator = GameObject.Find ("WaveIndicator");
		baseShield = GameObject.Find ("BaseShield").GetComponent<BaseShield>();
		waveData = new List<int[]>() {
			new int[10]{1,1,2,3,1,4,2,3,3,3},
			new int[20]{2,2,2,4,3,1,4,1,1,1,3,2,4,2,1,2,3,3,3,4},
			new int[30]{3,2,3,2,4,3,2,2,1,3,2,3,4,4,3,2,4,3,2,4,3,2,1,4,3,2,4,2,3,4},
			new int[40]{4,3,2,4,4,3,2,3,3,2,4,2,3,4,2,1,3,4,2,4,4,2,2,1,4,4,3,2,4,2,4,3,3,1,4,3,4,2,4,5},
			new int[50]{5,2,3,4,3,5,2,4,5,1,4,2,4,3,4,5,6,1,3,5,4,4,2,4,5,6,3,5,3,5,5,2,2,4,2,1,5,6,3,6,4,5,5,6,3,2,4,6,3,6}
		};
		waveNumber = 1;
		StartCoroutine ("waves");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!playerDead) {
			if (enemiesDestroyed == waveData[waveNumber].Length)
			{
				enemiesDestroyed = 0;
				waveNumber++;;
				StopCoroutine("waves");
				StartCoroutine ("waves");
			}
		} else {
			StopCoroutine("waves");
		}
	}
	void shipSpawn(int shipID)
	{
		switch (shipID)
		{ //spawn position will haave to be added
		case 1:
			GameObject lightFighter;
			lightFighter = Instantiate(enemyLightFighter, spawnPosition, Quaternion.identity) as GameObject;
			break;
		case 2:
			GameObject lightCruiser;
			lightCruiser = Instantiate(enemyLightCruiser,spawnPosition, Quaternion.identity) as GameObject;
			break;
		case 3:
			GameObject armouredFighter;
			armouredFighter = Instantiate(enemyArmouredFighter,spawnPosition,Quaternion.identity) as GameObject;
			break;
		case 4:
			GameObject bruiser;
			bruiser = Instantiate(enemyBruiser,spawnPosition,Quaternion.identity) as GameObject;
			break;
		case 5:
			GameObject heavyFighter;
			heavyFighter = Instantiate(enemyHeavyFighter,spawnPosition,Quaternion.identity) as GameObject;
			break;
		case 6:
			GameObject tank;
			tank = Instantiate(enemyTank,spawnPosition,Quaternion.identity) as GameObject;
			break;
		}
	}
	IEnumerator waves ()
	{
		baseShield.regenMultiplier = (100 - baseShield.shieldHitPoints)/20f;
		waveIndicator.GetComponent<SpriteRenderer>().sprite = waveImages[waveNumber-1];
		waveIndicator.transform.localScale = new Vector3(6f, 6f, 0f);
		waveIndicator.transform.position = new Vector3(0f, 0f, 0f);
		yield return new WaitForSeconds(4f);
		baseShield.regenMultiplier = 1f;
		waveIndicator.transform.localScale = new Vector3(1f, 1f, 0f);
		waveIndicator.transform.position = new Vector3(-6.82f, -12.6f, 0f);
		enemiesSpawned = 0;
		if (waveNumber < 6) {
			foreach(int ship in waveData[waveNumber])
			{
				if ((enemiesSpawned - enemiesDestroyed) < MAX_ENEMIES) {
					shipSpawn(ship);
					enemiesSpawned++;
				}
				yield return new WaitForSeconds (2f);
			}
		}
	}
	public void SpawnPowerup()
	{
		powerUpID = Random.Range (1,7);
		switch (powerUpID)
		{ //spawn position will have to be added
			case 1:
				GameObject doubleShotPU;
				doubleShotPU = Instantiate(doubleShot,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 2:
				GameObject powerShotPU;
				powerShotPU = Instantiate(powerShot,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 3:
				GameObject shieldRestorePU;
				shieldRestorePU = Instantiate(shieldRestore,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 4:
				GameObject hpRestorePU;
				hpRestorePU = Instantiate(hpRestore,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 5:
				GameObject quickShotPU;
				quickShotPU = Instantiate(quickShot,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 6:
				GameObject auxShieldPU;
				auxShieldPU = Instantiate(auxShield,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
			case 7:
				GameObject immunityShieldPU;
				immunityShieldPU = Instantiate(immunityShield,enemyDestroyLocation,Quaternion.identity) as GameObject;
				break;
		}
	}
}