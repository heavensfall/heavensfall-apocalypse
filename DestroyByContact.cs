using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour 
{

	public GameObject explosion;
	enemySpawner enemySpawn;
	PlayerHealth playerHealth;
	int	hitCounter = 0;
	int spawnChecker;
	int hitAlowance;

	void Start()
	{
		enemySpawn = GameObject.Find ("EnemySpawn").GetComponent <enemySpawner> ();
		if(GameObject.Find ("PlayerShip")!=null)
			playerHealth = GameObject.Find ("PlayerShip").GetComponent<PlayerHealth> ();
		switch (name)
		{
		case "EnemyArmouredFighter(Clone)":
			hitAlowance = 16;
			break;
		case "EnemyBruiser(Clone)":
			hitAlowance = 16;
			break;
		case "EnemyCruiser(Clone)":
			hitAlowance = 6;
			break;
		case "EnemyHeavyFighter(Clone)":
			hitAlowance = 32;
			break;
		case "EnemyLightFighter(Clone)":
			hitAlowance = 6;
			break;
		case "EnemyTank(Clone)":
			hitAlowance = 40;
			break;
		}


	}
	void Update()
	{
		if (hitCounter >= hitAlowance)
		{
			Instantiate(explosion, transform.position, transform.rotation);
			spawnChecker = Random.Range (4,6);
			if (spawnChecker == 5)
			{
				enemySpawn.enemyDestroyLocation = transform.position;
				enemySpawn.SpawnPowerup();
			}
			Destroy(gameObject);
			enemySpawn.enemiesDestroyed++;
		}
	}
	void OnTriggerEnter2D(Collider2D other) 
	{

		if (other.tag != "EnemyBullet")
		{
			if (other.tag == "Player") 
			{
				if (playerHealth !=null)
				{
					playerHealth.playerHitPoints = 0;
					hitCounter =hitCounter + 8;
				}
	

			}
			if(other.name == "playerBullet(Clone)")
			{
				hitCounter++;
			}
			if(other.name == "playerBulletRed(Clone)")
			{
				hitCounter =hitCounter +2;
			}
			Destroy(other.gameObject);
		}
	}
} 